<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \Firebase\JWT\JWT;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    //

    public function login(Request $Request)
    {
        $username = $request->username;
        $password = $request->password;
        if($password == 'admin' && $username == 'admin')
        {
            $key = env('SECRET_KEY');
            $payload = array(
                "iss" => "my_api",
                "aud" => "ur_apps",
                "username" => $username,
                "iat" => strtotime(date('Y-m-d H:i:s')),
                "nbf" => strtotime(date('2020-07-26 11:46:02')),
                "exp" => strtotime(date('2020-07-30 11:46:02'))
            );
            $jwt = JWT::encode($payload, $key);
            return response()->json($jwt);
        }
    }

    public function get_data()
    {
        $key = env('SECRET_KEY');    
        $token = $request->bearerToken();
        try{
            $decoded = JWT::decode($token, $key, array('HS256'));
            return response()->json($decoded->iat);
        }catch(\Firebase\JWT\ExpiredException $e){
            return response()->json($e->getMessage(), 403);
        }catch(\Firebase\JWT\BeforeValidException $e){
            return response()->json($e->getMessage(), 403);
        }catch(\Firebase\JWT\SignatureInvalidException $e){
            return response()->json($e->getMessage(), 403);
        }
    }
}
