<?php

/** @var \Laravel\Lumen\Routing\Router $router */
use \Firebase\JWT\JWT;
use Illuminate\Http\Request;


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () {    
    $key = env('SECRET_KEY');
    $payload = array(
        "iss" => "http://localhost/lumen/my_api",
        "aud" => "http://localhost/lumen/my_api",
        "iat" => strtotime(date('Y-m-d H:i:s')),
        "nbf" => strtotime(date('2020-07-26 11:46:02')),
        "exp" => strtotime(date('2020-07-30 11:46:02'))
    );
    $jwt = JWT::encode($payload, $key);
    return response()->json($jwt);
});

$router->get('/decode', function (Request $request) {
    $key = env('SECRET_KEY');    
    $token = $request->bearerToken();
    try{
        $decoded = JWT::decode($token, $key, array('HS256'));
        return response()->json($decoded->iat);
    }catch(\Firebase\JWT\ExpiredException $e){
        return response()->json($e->getMessage(), 403);
    }catch(\Firebase\JWT\BeforeValidException $e){
        return response()->json($e->getMessage(), 403);
    }catch(\Firebase\JWT\SignatureInvalidException $e){
        return response()->json($e->getMessage(), 403);
    }
});
